package com.eugenegolobokin.covid19app;

import com.eugenegolobokin.covid19app.model.stats.CountryStats;
import com.eugenegolobokin.covid19app.model.stats.SummaryStats;
import com.eugenegolobokin.covid19app.service.ConsoleUIService;
import com.eugenegolobokin.covid19app.service.DbReader;
import com.eugenegolobokin.covid19app.service.IOService;
import com.eugenegolobokin.covid19app.utils.StatHandler;

public class Covid19 {
    DbReader dbReader = new DbReader();
    ConsoleUIService uiService = new ConsoleUIService();
    StatHandler statHandler;
    SummaryStats summaryStats;
    String lastUpdateDate;

    public void run() {
        summaryStats = dbReader.receiveSummaryStats();
        lastUpdateDate = summaryStats.getCountries().get(0).getDate();
        uiService.showWelcomeMessage(lastUpdateDate, summaryStats.getGlobal().getNewConfirmed());
        statHandler = new StatHandler(summaryStats.getCountries());

        while (true) {
            uiService.showMenu();
            selectOption();
        }

    }

    public void selectOption() {
        int answer = IOService.getUserInputNumber();
        switch (answer) {
            case 0:
                shutDown();
                break;
            case 1:
                runLatestStatisticsSummary();
                break;
            case 2:
                runCountryWithHighestCases();
                break;
            case 3:
                runStatsBySearchingCountry();
                break;
            case 4:
                runSelectionOfTopCountriesByCases();
                break;
            case 5:
                runCountryFullStats();
                break;
            default:
                uiService.displayMessage("%n%s%n", "   Please enter valid menu number!");
        }
    }

    private void runCountryFullStats() {
        uiService.displayMessage("%n%s%n", "   Enter full or part of country name:");
        String slug = summaryStats.getSlugByCountryName();
        CountryStats[] cs = dbReader.receiveCountryFullStats(slug);
        statHandler.printCountryFullStats(cs);
    }

    private void runSelectionOfTopCountriesByCases() {
        uiService.displayMessage("%n%s%n", "   Select how many countries with highest amount of cases to display:");
        statHandler.printTopInfectedCountries();
    }

    private void runStatsBySearchingCountry() {
        uiService.displayMessage("%n%s%n%s%n",
                "   Search countries by key",
                "   Please enter at least 3 characters to start search:");
        statHandler.getCountryByKeyword();
    }

    private void runCountryWithHighestCases() {
        uiService.displayMessage("%n%s%n", "   Country with highest amount of total cases:");
        statHandler.getCountryWithHighestTotalCases();
    }

    private void runLatestStatisticsSummary() {
        System.out.println(summaryStats.getGlobal());
    }

    private void shutDown() {
        System.exit(0);
    }

}
