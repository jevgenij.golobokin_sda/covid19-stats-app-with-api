package com.eugenegolobokin.covid19app.utils;

import com.eugenegolobokin.covid19app.model.Country;
import com.eugenegolobokin.covid19app.model.stats.CountryStats;
import com.eugenegolobokin.covid19app.service.IOService;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class StatHandler {

    List<Country> countries;

    public StatHandler(List<Country> countries) {
        this.countries = countries;
    }

    public void getCountryByKeyword() {
        String key = IOService.getSearchKeyword();
        countries.stream()
                .filter(c -> c.getCountryName().toLowerCase().contains(key))
                .map(Country::toString)
                .forEach(System.out::println);
    }


   /* public void getCountryByKeyword(List<Country> countries) {
        String key = IOService.getUserInput();
        countries.forEach(country -> {
            if (country.getCountryName().contains(key)) {
                System.out.println(country);
            }
        });
    }*/

    public void getCountryWithHighestTotalCases() {
        countries
                .stream()
                .max(Comparator.comparingInt(Country::getTotalConfirmed))
                .ifPresent(System.out::println);
    }

    public void printTopInfectedCountries() {
        int requestedNumberOfCountries = IOService.getUserInputNumber();
        AtomicInteger x = new AtomicInteger();
        countries.stream()
                .sorted(Comparator.reverseOrder())
                .limit(requestedNumberOfCountries)
                .forEach(c -> System.out.println((x.getAndIncrement() + 1) + ". " + c));
    }

    public void printCountryFullStats(CountryStats[] cs) {
        System.out.printf("   Full statistics for %s by date:%n", cs[0].getCountryName());
        Arrays.stream(cs).forEach(System.out::println);
    }

}
