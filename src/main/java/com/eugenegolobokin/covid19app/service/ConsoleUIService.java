package com.eugenegolobokin.covid19app.service;


public class ConsoleUIService {

    public void displayMessage(String message, Object... args) {
        System.out.printf(message + "\n", args);
    }

    public void showWelcomeMessage(String lastUpdateDate, int newConfirmedCases) {
        displayMessage("%n%s", "---------- WELCOME TO THE COVID19 STATISTICS APP!! ----------");
        displayMessage("%n%s: %s", "   Latest stats database update: ", lastUpdateDate);
        displayMessage("%s: %d", "   Total new cases conformed today: ", newConfirmedCases);
    }

    public void showMenu() {
        displayMessage("%n%s%n%n%s%n%s%n%s%n%s%n%s%n%s%n%n%s",
                "   Select option from menu:",
                "1. Print latest statistics summary",
                "2. Show country with highest number of total cases",
                "3. Show stats by searching country",
                "4. Show selected top countries by highest number of cases",
                "5. Show stats of selected country by period",
                "0. Exit program",
                "   Enter number to proceed:");
    }


}
