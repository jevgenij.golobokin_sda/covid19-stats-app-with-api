package com.eugenegolobokin.covid19app.service;

import java.util.Scanner;

public class IOService {

    private static Scanner scanner = new Scanner(System.in);

    public static String getUserInput() {
        return scanner.next();
    }

    public static int getUserInputNumber() {
        int inputNumber;
        try {
            inputNumber = Integer.parseInt(scanner.next());
        } catch (NumberFormatException e) {
            System.err.println("Please enter number!");
            return getUserInputNumber();
        }
        return inputNumber;
    }

    public static String getSearchKeyword() {
        String userInput = scanner.next();
        if (userInput.length() < 3) {
            System.err.println("Please enter at least 3 characters to start search!");
            return getSearchKeyword();
        } else {
            return userInput;
        }
    }


}
