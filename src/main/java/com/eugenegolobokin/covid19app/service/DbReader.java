package com.eugenegolobokin.covid19app.service;

import com.eugenegolobokin.covid19app.model.stats.CountryStats;
import com.eugenegolobokin.covid19app.model.stats.SummaryStats;
import com.eugenegolobokin.covid19app.network.RequestUrlData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class DbReader {
    private final String SUMMARY_STATS_URL = "https://api.covid19api.com/summary";
    private final String COUNTRY_STATS_URL_FIRST_PART = "https://api.covid19api.com/total/country/";
    private final String COUNTRY_STATS_URL_SECOND_PART = "/status/confirmed";


    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public SummaryStats receiveSummaryStats() {
        return gson.fromJson(RequestUrlData.getHttpResponse(SUMMARY_STATS_URL), SummaryStats.class);
    }

    public CountryStats[] receiveCountryFullStats(String slug) {
        String url = COUNTRY_STATS_URL_FIRST_PART + slug + COUNTRY_STATS_URL_SECOND_PART;
        return gson.fromJson(RequestUrlData.getHttpResponse(url), CountryStats[].class);
    }

}
