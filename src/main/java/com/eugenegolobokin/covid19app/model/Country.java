package com.eugenegolobokin.covid19app.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Country implements Comparable<Country> {
    @SerializedName("Country")
    private String countryName;
    @SerializedName("CountryCode")
    private String countryCode;
    @SerializedName("Slug")
    private String slug;
    @SerializedName("NewConfirmed")
    private int newConfirmed;
    @SerializedName("TotalConfirmed")
    private int totalConfirmed;
    @SerializedName("NewDeaths")
    private int newDeaths;
    @SerializedName("TotalDeaths")
    private int totalDeaths;
    @SerializedName("NewRecovered")
    private int newRecovered;
    @SerializedName("TotalRecovered")
    private int totalRecovered;
    @SerializedName("Date")
    private Date date;

    public Country() {
    }

    public String getCountryName() {
        return countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getSlug() {
        return slug;
    }

    public int getNewConfirmed() {
        return newConfirmed;
    }

    public int getTotalConfirmed() {
        return totalConfirmed;
    }

    public int getNewDeaths() {
        return newDeaths;
    }

    public int getTotalDeaths() {
        return totalDeaths;
    }

    public int getNewRecovered() {
        return newRecovered;
    }

    public int getTotalRecovered() {
        return totalRecovered;
    }

    public String getDate() {
        return new SimpleDateFormat("dd-MM-yyyy, kk:mm").format(this.date);
    }

    @Override
    public String toString() {
        return String.format("%s has %d new cases and %d total confirmed cases.",
                this.countryName, this.newConfirmed, this.totalConfirmed);
    }


    @Override
    public int compareTo(Country country) {
        return Integer.compare(this.totalConfirmed, country.totalConfirmed);
    }


}
