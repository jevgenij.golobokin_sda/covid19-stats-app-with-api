package com.eugenegolobokin.covid19app.model.stats;

import com.eugenegolobokin.covid19app.model.Country;
import com.eugenegolobokin.covid19app.model.Global;
import com.eugenegolobokin.covid19app.service.IOService;
import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

public class SummaryStats {

    @SerializedName("Global")
    private Global global;
    @SerializedName("Countries")
    private List<Country> countries;

    public SummaryStats() {
    }

    public Global getGlobal() {
        return global;
    }

    public List<Country> getCountries() {
        return Collections.unmodifiableList(countries);
    }

    public String getSlugByCountryName() {
        String slugInput = IOService.getUserInput();
        return countries.stream()
                .filter(c -> c.getCountryName().toLowerCase().contains(slugInput))
                .map(Country::getSlug)
                .findAny()
                .orElse("0");
    }
}
