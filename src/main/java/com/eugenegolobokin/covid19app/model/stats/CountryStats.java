package com.eugenegolobokin.covid19app.model.stats;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CountryStats {
    @SerializedName("Country")
    private String countryName;

    @SerializedName("Cases")
    private int cases;

    @SerializedName("Date")
    private Date date;

    public int getCases() {
        return cases;
    }

    public String getCountryName() {
        return countryName;
    }

    public String getDate() {
        return new SimpleDateFormat("dd-MM-yyyy").format(this.date);
    }

    @Override
    public String toString() {
        return String.format("%s confirmed cases: %d", getDate(), this.cases);
    }

}
