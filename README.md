# COVID19 stats tracker

### About the app

**COVID19 stats tracker** is a small console app for getting various Covid19 statistics summaries using covid19api.com data.

*App menu:*
1. Print latest statistics summary
2. Show country with the highest number of total cases
3. Show stats by searching country
4. Show selected top countries by highest number of cases
5. Show stats of selected country by period

### Demo

![](demo.gif)

### How to run
This is a Maven project with console UI
1. Clone or download project
2. Run Main.java

### Tech used
*Java 13, Apache Maven, Google gson 2.8.6, Covid19 API (covid19api.com)*

